# ---
require essioc
require lakeshore224

epicsEnvSet(IPADDR, "172.30.32.12")
epicsEnvSet(IPPORT, "7777")
epicsEnvSet(PREFIX, "SE-SEE:SE-LS224-001")
epicsEnvSet(LOCATION, "SE: $(IPADDR):$(IPPORT)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(lakeshore224_DIR)db")

iocshLoad("$(essioc_DIR)/common_config.iocsh")
iocshLoad("$(lakeshore224_DIR)lakeshore224.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

seq install_curve, "P=$(PREFIX), CurvePrefix=File"

#- Uncomment to debug
#- asynSetTraceMask($(PREFIX), -1, 0xF)
#- asynSetTraceIOMask($(PREFIX), -1, 0x2)
# ...                 
